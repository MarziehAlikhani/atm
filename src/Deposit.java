public class Deposit extends Transaction {
    private Double amount;
    private GetInput getInput;

    public Deposit(int accountNumber, BankDataBase bankDataBase, ShowOutPut showOutPut, GetInput getInput , TransactionType transactionType) {
        super(accountNumber, bankDataBase, showOutPut , transactionType);
        this.getInput = getInput;
    }

    @Override
    public void execute() {

        amount = getDepositAmountFromUser();
        if (amount != 0){
            getBankDataBase().credit(getAccountNumber(), amount);
            getBankDataBase().setTransaction(getAccountNumber(),this);

            getShowOutPut().showMessage("Your balance added");
            getShowOutPut().showAmount(amount);
        }else{
            getShowOutPut().showMessage("\nCancelling transaction...");
        }

    }

    private Double getDepositAmountFromUser() {
        getShowOutPut().showMessage("please enter a deposit amount");
        getShowOutPut().showMessage("Enter 0 for cancel...");

        Double requestedAmount = getInput.getDouble();
        return requestedAmount;
    }

    @Override
    public Double getAmount() {
        return amount;
    }
}
