import java.util.List;

public class BankDataBase {

    Person person = new Person(1,"Marzieh" , "Alikhani" , "1272697053");
    Account account1 = new Account(12345, 12345, 50_000, 100_000 , person);
    List<Account> accountList = List.of(account1);


    public boolean authenticateUser(int accountNumber, int password) {
        Account userAccount = findAccount(accountNumber);

        if (userAccount != null) {
            return userAccount.validatePassword(password);
        } else {
            return false;
        }

    }

    private Account findAccount(int accountNumber) {
        for (Account account : accountList) {
            if (account.getAccountNumber() == accountNumber) {
                return account;
            }
        }
        return null;
    }

    public double getAvailableBalance(int userAccountNumber) {
        return findAccount(userAccountNumber).getAvailableBalance();
    }

    public double getTotalBalance(int userAccountNumber) {
        return findAccount(userAccountNumber).getTotalBalance();
    }

    public void debit(int userAccountNumber, double amount) {
        findAccount(userAccountNumber).debit(amount);
    }

    public void credit(int userAccountNumber, double amount){
        findAccount(userAccountNumber).credit(amount);
    }

    public void setTransaction(int userAccountNumber ,Transaction transaction){
        findAccount(userAccountNumber).setTransactionList(transaction);
    }

    public List<Transaction> getAccountTransactions(int userAccountNumber){
        List<Transaction> transactions = findAccount(userAccountNumber).getTransactionList();
        return transactions;
    }

}
