import java.util.ArrayList;
import java.util.List;

public class Account {

    private int accountNumber;
    private int password;
    private double availableBalance;
    private double totalBalance;
    private Person person;
    private List<Transaction> transactionList;
    public Account(int accountNumber, int password, double availableBalance, double totalBalance , Person person) {
        this.accountNumber = accountNumber;
        this.password = password;
        this.availableBalance = availableBalance;
        this.totalBalance = totalBalance;
        this.person = person;
        this.transactionList = new ArrayList<>();
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getPassword() {
        return password;
    }

    public double getAvailableBalance() {
        return availableBalance;
    }

    public double getTotalBalance() {
        return totalBalance;
    }

    public Person getPerson() {
        return person;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(Transaction transaction) {
        this.transactionList.add(transaction);
    }

    public boolean validatePassword(int enterPassword){
        if(enterPassword == password)
            return true;
        else
            return false;
    }

    public void credit(double amount) {
        totalBalance += amount;
        availableBalance += amount;
    }

    public void debit(double amount) {
        availableBalance -= amount;
        totalBalance -= amount;
    }

}
