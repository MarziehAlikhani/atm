import java.util.List;

public class ATM {
    private boolean userAuthenticated;
    private int currentAccountNumber;
    private ShowOutPut showOutPut;
    private GetInput getInput;
    private BankDataBase bankDataBase;

    public ATM() {
        userAuthenticated = false;
        currentAccountNumber = 0;
        showOutPut = new ShowOutPut();
        getInput = new GetInput();
        bankDataBase = new BankDataBase();
    }

    public void run() {
        while (true) {
            while (!userAuthenticated) {
                showOutPut.showMessage("\nwelcome");
                authenticateUser();
            }
            doTransaction();
            userAuthenticated = false;
            currentAccountNumber = 0;
            showOutPut.showMessage("\nThank you! Goodbye!");
        }
    }

    public void doTransaction() {
        Transaction currentTransaction = null;
        boolean exit = false;

        while (!exit) {
            int selectedItemOfMenu = showMenu();

            switch (selectedItemOfMenu) {
                case 1, 2, 3:
                    currentTransaction = createTransaction(selectedItemOfMenu);
                    currentTransaction.execute();
                    break;
                case 4:
                    List<Transaction> transactions = bankDataBase.getAccountTransactions(currentAccountNumber);
                    showOutPut.showTransactionList(transactions);
                    System.out.println(transactions);
                    break;
                case 5:
                    showOutPut.showMessage("\nExiting the system...");
                    exit = true;
                    break;
                default:
                    showOutPut.showMessage("\nYou did not enter a valid selection. Try again.");
                    break;
            }
        }

    }

    public int showMenu() {
        showOutPut.showMessage("\nMain menu:");
        showOutPut.showMessage("1 - View my balance");
        showOutPut.showMessage("2 - Withdraw cash");
        showOutPut.showMessage("3 - Deposit funds");
        showOutPut.showMessage("4 - Get Transaction List");
        showOutPut.showMessage("5 - Exit\n");
        showOutPut.showMessage("Enter a choice: ");
        return getInput.getSc();
    }

    private void authenticateUser() {
        showOutPut.showMessage("Please enter your account number:");
        int accountNumber = getInput.getSc();
        showOutPut.showMessage("Please enter your password");
        int password = getInput.getSc();

        userAuthenticated = bankDataBase.authenticateUser(accountNumber, password);
        if (userAuthenticated) {
            currentAccountNumber = accountNumber;
        } else {
            showOutPut.showMessage("Invalid account number or password , please try again.");
        }
    }

    private Transaction createTransaction(int selectedItemOfMenu) {
        Transaction currentTransaction = null;
        switch (selectedItemOfMenu) {
            case 1:
                currentTransaction = new BalanceInquiry(currentAccountNumber, bankDataBase, showOutPut , TransactionType.BALANCE_INQUIRY);
                break;
            case 2:
                currentTransaction = new WithDrawl(currentAccountNumber, bankDataBase, showOutPut, getInput , TransactionType.WITH_DRAWL);
                break;
            case 3:
                currentTransaction = new Deposit(currentAccountNumber, bankDataBase, showOutPut, getInput , TransactionType.DEPOSIT);
                break;
        }
        return currentTransaction;
    }
}
