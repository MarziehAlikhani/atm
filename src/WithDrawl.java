public class WithDrawl extends Transaction {

    private Double amount;
    private GetInput getInput;

    public WithDrawl(int accountNumber, BankDataBase bankDataBase, ShowOutPut showOutPut, GetInput getInput , TransactionType transactionType) {
        super(accountNumber, bankDataBase, showOutPut , transactionType);
        this.getInput = getInput;
    }

    @Override
    public void execute() {
        amount = showMenuOfAmount();

        if (amount != 0) {
            double availableBalance = getBankDataBase().getAvailableBalance(getAccountNumber());
            if (amount <= availableBalance) {
                getBankDataBase().debit(getAccountNumber(), amount);
                getBankDataBase().setTransaction(getAccountNumber(),this);

                getShowOutPut().showMessage("\nYour cash has been dispensed. Please take your cash now.");
            }else{
                getShowOutPut().showMessage("\nInsufficient funds in your account. \nPlease choose a smaller amount.");
            }
        }else{
            getShowOutPut().showMessage("\nCancelling transaction...");
        }

    }

    private Double showMenuOfAmount() {

        Double selectedAmount = 0.0;

        ShowOutPut showOutPut = getShowOutPut();


        Double[] amounts = {0.0, 20.0, 40.0, 60.0, 100.0, 200.0};
        int input = 0;

        while (selectedAmount == 0 && input != 6) {
            showOutPut.showMessage("\nWithdrawl Menu:");
            showOutPut.showMessage("1 - $20");
            showOutPut.showMessage("2 - $40");
            showOutPut.showMessage("3 - $60");
            showOutPut.showMessage("4 - $100");
            showOutPut.showMessage("5 - $200");
            showOutPut.showMessage("6 - Cancel transaction");
            showOutPut.showMessage("\nChoose a withdrawl amount: ");

             input = getInput.getSc();

            switch (input) {
                case 1 , 2 , 3 , 4 , 5:
                    selectedAmount = amounts[input];
                    break;
                case 6:
                    selectedAmount =0.0;
                    break;
                default:
                    showOutPut.showMessage("\nInvalid selection, Try again.");
            }
        }
        return selectedAmount;
    }

    @Override
    public Double getAmount() {
        return amount;
    }
}
