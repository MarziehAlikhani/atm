public class BalanceInquiry extends Transaction{
    public BalanceInquiry(int accountNumber, BankDataBase bankDataBase, ShowOutPut showOutPut , TransactionType transactionType) {
        super(accountNumber, bankDataBase, showOutPut , transactionType);
    }

    @Override
    public void execute() {
        ShowOutPut showOutPut = new ShowOutPut();

        double totalBalance = getBankDataBase().getTotalBalance(getAccountNumber());

        double availableBalance = getBankDataBase().getAvailableBalance(getAccountNumber());

        showOutPut.showMessage("\nBalance information:");
        showOutPut.showMessage("\nTotal Balance:");
        showOutPut.showAmount(totalBalance);
        showOutPut.showMessage("\nAvailable Balance:");
        showOutPut.showAmount(availableBalance);
    }
}
