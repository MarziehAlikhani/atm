import java.util.Scanner;

public class GetInput {
    private Scanner sc;


    public GetInput() {
        sc = new Scanner(System.in);
    }

    public int getSc() {
        return sc.nextInt();
    }
    public double getDouble(){
        return sc.nextDouble();
    }
}
