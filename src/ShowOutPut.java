import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ShowOutPut {
    public void showMessage(String message){
        System.out.println(message);
    }
    public void showAmount(double amount){
        System.out.printf("$%,.2f", amount);
    }

    public void showTransactionList(List<Transaction> transactions){
        System.out.println("\nTransaction List");
        AtomicInteger index = new AtomicInteger(1);
        transactions.forEach(transaction->{
            System.out.println(index.get());
            System.out.println("Account Number:" + transaction.getAccountNumber());
            System.out.println("Creation Date:" + transaction.getDate());
            System.out.println("Transaction Type:" + transaction.getTransactionType());
            System.out.println("Transaction amount:" + transaction.getAmount());
            System.out.println("---------------------------------------------");
            index.getAndIncrement();
        });
    }
}
