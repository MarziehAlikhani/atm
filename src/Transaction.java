import java.util.Date;

public abstract class Transaction {
    private int accountNumber;
    private Date date;
    private BankDataBase bankDataBase;
    private ShowOutPut showOutPut;
    private TransactionType transactionType;
    private Double amount;

    public Transaction(int accountNumber, BankDataBase bankDataBase, ShowOutPut showOutPut , TransactionType transactionType) {
        this.accountNumber = accountNumber;
        this.bankDataBase = bankDataBase;
        this.showOutPut = showOutPut;
        this.date = new Date();
        this.transactionType = transactionType;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public BankDataBase getBankDataBase() {
        return bankDataBase;
    }

    public ShowOutPut getShowOutPut() {
        return showOutPut;
    }

    public Date getDate() {
        return date;
    }

    public Double getAmount() {
        return amount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    abstract public void execute();

}

